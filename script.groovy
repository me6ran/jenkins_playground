def buildApp() {
    echo 'building the application...'
} 

def testApp() {
    echo 'testing the application...'
} 

def deployApp() {
    echo 'deplying the application...'
    echo "deploying version ${params.VERSION}"
} 

def checkCVersions(String programmingLanguage){
        def sout = new StringBuilder(), serr = new StringBuilder()
        def proc = "$programmingLanguage --version".execute()
        proc.consumeProcessOutput(sout, serr)
        proc.waitForOrKill(1000)
        println "out> $sout\nerr> $serr"
}

String locateUpstream(){
    def upstream = currentBuild.rawBuild.getCause(hudson.model.Cause$UpstreamCause)
    println "upstream shortdesc: ${upstream?.shortDescription}" 
    println "upstream getUpstreamUrl(): ${upstream?.getUpstreamUrl()}"
    println "upstream getUpstreamProject(): ${upstream?.getUpstreamProject()}"
    println "upstream toString(): ${upstream?.toString()}" 
    return upstream?.getUpstreamProject()
}


return this
