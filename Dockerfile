FROM python:3.7 as py3
FROM jenkins/jenkins:lts-jdk11
#FROM python:3.7

COPY --from=py3 /usr/local/lib /usr/local/lib
COPY --from=py3 /usr/local/bin /usr/local/bin
COPY --from=py3 /usr/local/include /usr/local/include
COPY --from=py3 /usr/local/man /usr/local/man
COPY --from=py3 /usr/local/share /usr/local/share
#RUN python -m pip install requests